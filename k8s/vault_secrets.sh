#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat <<END
    This script is intended to be run by other scripts
    The purpose of this script is to retrieve the vault secrets to set up nginx load balancer for an envrionment.

    Arguments
        ENVIRONMENT (Required) = The environment name that should be specified for the location in vault of where to look up secrets from.
END
)
echo "$HELPTEXT"
}

if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

ENVIRONMENT=$1

case $ENVIRONMENT in
     dev)
          ;;
     staging)
          ;;
     prod)
          ;;
     *)
          echo "ERROR: The environment $ENVIRONMENT passed in doesn't match a known environment in the script."
          exit 1
          ;;
esac

CIDR_IP_WHITELISTING=$(vault kv get --field=allowed /secret/environments/$ENVIRONMENT/cidr_authorization | sed 's/,/\\n  - /g')
CIDR_IP_WHITELISTING=$(echo "- $CIDR_IP_WHITELISTING")

sed -z "s|\${CIDR_IP_WHITELISTING}|$CIDR_IP_WHITELISTING|g" nginx-load-balancer.yaml.template > generated/nginx-load-balancer.yaml

echo "Created CIDR_IP_WHITELISTING in nginx-load-balancer.yaml."
