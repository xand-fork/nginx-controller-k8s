# NGINX Ingress Controller K8s

## Requirements
- `kustomize`
- `kubectl`

## Quick Start
To build, enter the `k8s` directory
```
cd k8s
```
and run `kustomize`
```
kustomize build
```
Apply to your Kubernetes cluster using `kubectl`:
```
kubectl apply -f -
```
